def fib( ):
    x,y = 0,1
    while True:
        yield x
        x,y = y, x+y
def even( seq ):
    for number in seq:
        if not number % 2:
            yield number
def under( seq, the_max ):
    for number in seq:
        if number > the_max:
            break
        yield number
print sum( even( under( fib( ), 4000000 ) ) )
